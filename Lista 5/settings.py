DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'lista5db',
        'USER': 'admindb',
        'PASSWORD': 'mlista5password',
        'HOST': 'db',
        'PORT': 5432,
    }
}
