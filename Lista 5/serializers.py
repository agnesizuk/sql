#add mask on password, should be pass on ,due to 04.05.23

from rest_framework import serializers
from .models import User, Post, Comment

class MaskedPasswordField(serializers.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['style'] = {'input_type': 'password'}
        super().__init__(*args, **kwargs)

class UserSerializer(serializers.ModelSerializer):
    password = MaskedPasswordField(write_only=True)
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'password']

class PostSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    class Meta:
        model = Post
        fields = ['id', 'title', 'content', 'created_at', 'updated_at', 'author']

class CommentSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    post = PostSerializer(read_only=True)
    class Meta:
        model = Comment
        fields = ['id', 'content', 'created_at', 'updated_at', 'author', 'post']
