//Adding references
db.orders.updateMany({}, {$unset: {product: 1, customer: 1}}) 
db.orders.updateMany({product_id: null}, {$lookup: {from: "products", localField: "product", foreignField: "name", as: "product_info"}}) 
db.orders.updateMany({customer_id: null}, {$lookup: {from: "customers", localField: "customer", foreignField: "name", as: "customer_info"}})
db.orders.updateMany({product_info: {$size: 1}}, {$set: {product_id: {$arrayElemAt: ["$product_info._id", 0]}}}) 
db.orders.updateMany({customer_info: {$size: 1}}, {$set: {customer_id: {$arrayElemAt: ["$customer_info._id", 0]}}}) 

//Add index in collection 
db.orders.createIndex({product_id: 1})
db.orders.createIndex({customer_id: 1})

//Output do database in mongoDB 

db.orders.aggregate([
    {
        $lookup: {
            from: "products",
            localField: "product_id",
            foreignField: "_id",
            as: "product_info"
        }
    },
    {
        $lookup: {
            from: "customers",
            localField: "customer_id",
            foreignField: "_id",
            as: "customer_info"
        }
    },
    {
        $project: {
            _id: 1,
            quantity: 1,
            total: 1,
            "product_info.name": 1,
            "product_info.price": 1,
            "customer_info.name": 1,
            "customer_info.email": 1,
            "customer_info.phone": 1
        }
    }
])
