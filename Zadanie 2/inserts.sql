    INSERT INTO Pasazerowie (imie, nazwisko, nr_telefonu, email) VALUES
    ('Jan', 'Gabrowski', '123456789', 'jan.Gabrowski@example.com'),
    ('Anna', 'Guderski', '987654321', 'anna.Guderski@example.com'),
    ('Adam', 'Szymańska', '555222444', 'adam.slowacki@example.com'),
    ('Maria', 'Kuczma', '111333555', 'maria.wojcik@example.com'),
    ('Michał', 'Deralksa', '777888999', 'michal.Deralksa@example.com');


    INSERT INTO Loty (nr_lotu, data_lotu, czas_wylotu, czas_przylotu, skad, dokad, nr_samolotu) VALUES
    ('LO123', '2023-04-01', '12:00:00', '14:00:00', 'Warszawa', 'Kraków', 'SP-LIC'),
    ('LO456', '2023-04-21', '16:30:00', '18:30:00', 'Kraków', 'Gdańsk', 'SP-LID'),
    ('LO789', '2023-05-03', '07:45:00', '10:00:00', 'Wrocław', 'Paryż', 'SP-LIB'),
    ('LO101', '2023-05-12', '09:00:00', '12:30:00', 'Gdańsk', 'Londyn', 'SP-LIF'),
    ('LO112', '2023-06-05', '20:15:00', '23:30:00', 'Kraków', 'Madryt', 'SP-LIC');

    INSERT INTO Samoloty (nr_samolotu, model, liczba_miejsc, typ) VALUES
    ('SP-LIC', 'Airbus A320', 180, 'pasażerski'),
    ('SP-LID', 'Boeing 737', 160, 'pasażerski'),
    ('SP-LIE', 'Embraer 190', 100, 'pasażerski'),
    ('SP-LIF', 'Boeing 787', 250, 'pasażerski'),
    ('SP-LIG', 'Boeing 777', 300, 'pasażerski');


    INSERT INTO Zaloga (id_lotu, nr_samolotu, id_pilota, id_stewardess) VALUES
    (1, 'SP-LIC', 1, 2),
    (2, 'SP-LID', 2, 3),
    (3, 'SP-LIE', 3, 4),
    (4, 'SP-LIF', 4, 5),
    (5, 'SP-LIG', 5, 1);

    INSERT INTO Bagaze (id_pasażera, nr_lotu, waga_pasażera, waga_bagazu_rejestrowanego, wymiary_bagazu_podrecznego) VALUES
    (1, 'LO123', 80, 20, '50x40x
