CREATE TABLE Pasażer (
    ID_Pasażera INT PRIMARY KEY,
    Imię VARCHAR(50),
    Nazwisko VARCHAR(50),
    Adres VARCHAR(100)
);

CREATE TABLE Samolot (
    ID_Samolotu INT PRIMARY KEY,
    Model VARCHAR(50),
    Liczba_miejsc INT,
    Typ VARCHAR(50)
);

CREATE TABLE Rezerwacja (
    ID_Rezerwacji INT PRIMARY KEY,
    ID_Pasażera INT,
    ID_Lotu INT,
    Numer_miejsca INT,
    Typ_bagażu VARCHAR);


______________________
Opis :


Table: Pasazer
Columns: 
- pasazer_id (primary key)
- imie
- nazwisko
- nr_telefonu
- email

Table: Lot
Columns: 
- lot_id (primary key)
- skad
- dokad
- czas_wylotu
- czas_przylotu

Table: Rezerwacja
Columns:
- rezerwacja_id (primary key)
- lot_id (foreign key to Lot)
- pasazer_id (foreign key to Pasazer)

Table: Samolot
Columns: 
- samolot_id (primary key)
- typ

Table: Personel
Columns:
- personel_id (primary key)
- samolot_id (foreign key to Samolot)

Table: Pilot
Columns:
- pilot_id (primary key)
- imie
- nazwisko
- uprawnienia

Table: Stewardesa
Columns:
- stewardesa_id (primary key)
- imie
- nazwisko

Table: Bagaż
Columns:
- bagaz_id (primary key)
- pasazer_id (foreign key to Pasazer)
- typ (podreczny lub rejestrowany)
- waga
- rozmiar
