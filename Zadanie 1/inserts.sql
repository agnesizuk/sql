-- Insert do tabeli Teacher
INSERT INTO Teacher (TNO, TNAME, TITLE, CITY, supervisor)
VALUES (1, 'Gawleski', 'dr inż.', 'Warszawa', NULL);

INSERT INTO Teacher (TNO, TNAME, TITLE, CITY, supervisor)
VALUES (2, 'Dudek', 'mgr', 'Kraków', 1);

INSERT INTO Teacher (TNO, TNAME, TITLE, CITY, supervisor)
VALUES (3, 'Guderski', 'profesor', 'Gdańsk', NULL);

INSERT INTO Teacher (TNO, TNAME, TITLE, CITY, supervisor)
VALUES (4, 'Wójcik', 'inż.', 'Łódź', 3);

INSERT INTO Teacher (TNO, TNAME, TITLE, CITY, supervisor)
VALUES (5, 'Dombrek', 'mgr', 'Katowice', 3);


-- Insert do tabeli Student
INSERT INTO Student (SNO, SNAME, SYEAR, CITY)
VALUES (1, 'Dudek', 2018, 'Kraków');

INSERT INTO Student (SNO, SNAME, SYEAR, CITY)
VALUES (2, 'Gawleski', 2019, 'Warszawa');

INSERT INTO Student (SNO, SNAME, SYEAR, CITY)
VALUES (3, 'Guderski', 2017, 'Gdańsk');

INSERT INTO Student (SNO, SNAME, SYEAR, CITY)
VALUES (4, 'Wójcik', 2018, 'Łódź');

INSERT INTO Student (SNO, SNAME, SYEAR, CITY)
VALUES (5, 'Dombrek', 2019, 'Katowice');


-- Insert do tabeli Subject
INSERT INTO Subject (CNO, CNAME, STUDYEAR)
VALUES (1, 'Plastyka', 2018);

INSERT INTO Subject (CNO, CNAME, STUDYEAR)
VALUES (2, 'Informatyka', 2019);

INSERT INTO Subject (CNO, CNAME, STUDYEAR)
VALUES (3, 'Fizyka', 2017);

INSERT INTO Subject (CNO, CNAME, STUDYEAR)
VALUES (4, 'Chemia', 2018);

INSERT INTO Subject (CNO, CNAME, STUDYEAR)
VALUES (5, 'Biologia', 2019);


-- Insert do tabeli Course
INSERT INTO Course (TNO, CNO, HOURS, GRADE)
VALUES (1, 1, 30, 5);

INSERT INTO Course (TNO, CNO, HOURS, GRADE)
VALUES (2, 2, 45, 4);

INSERT INTO Course (TNO, CNO, HOURS, GRADE)
VALUES (3, 3, 60, 3);

INSERT INTO Course (TNO, CNO, HOURS, GRADE)
VALUES (4, 4, 75, 2);

INSERT INTO Course (TNO, CNO, HOURS, GRADE)
VALUES (5, 5, 90, 5);


-- Widok
CREATE VIEW FreeSeats AS
SELECT Flight.flight_number, Plane.type, Plane.capacity - COUNT(Ticket.seat_number) AS free_seats
FROM Flight
JOIN
