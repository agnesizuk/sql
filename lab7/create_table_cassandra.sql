CREATE TABLE warehouse (
  warehouse_id UUID PRIMARY KEY,
  clothing_id UUID,
  size TEXT,
  quantity INT
);

CREATE TABLE shop (
  shop_id UUID PRIMARY KEY,
  clothing_id UUID,
  price DECIMAL,
  quantity_sold INT
);

CREATE TABLE staff (
  staff_id UUID PRIMARY KEY,
  name TEXT,
  position TEXT
);

CREATE TABLE customers (
  customer_id UUID PRIMARY KEY,
  name TEXT,
  email TEXT,
  address TEXT
);

CREATE TABLE wholesalers (
  wholesaler_id UUID PRIMARY KEY,
  name TEXT,
  email TEXT,
  address TEXT
);

CREATE TABLE discount_codes (
  code TEXT PRIMARY KEY,
  percentage INT
)
