-- Query 1: 
SELECT clothing_id, size, SUM(quantity_sold) AS total_sold
FROM shop
GROUP BY clothing_id, size
ORDER BY total_sold DESC;

-- Query 2:
SELECT clothing_id, SUM(quantity_sold * price) AS total_profit
FROM shop
GROUP BY clothing_id
ORDER BY total_profit DESC;

-- Query 3: 
SELECT clothing_id
FROM shop
WHERE category = 'given_category';

-- Query 4:
SELECT staff_id, SUM(quantity_sold * price) AS total_profit, SUM(quantity_sold) AS total_sold
FROM shop
GROUP BY staff_id
ORDER BY total_profit DESC, total_sold DESC
LIMIT 1;

-- Query 5
SELECT customer_id, SUM(total_purchase) AS total_spent
FROM (
  SELECT customer_id, SUM(price * quantity_sold) AS total_purchase
  FROM shop
  GROUP BY customer_id, price
) AS customer_purchase
GROUP BY customer_id
ORDER BY total_spent DESC
LIMIT 3;
