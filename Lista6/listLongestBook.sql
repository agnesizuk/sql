FOR l IN Loan
    FILTER l.returned_date != null
    LET checkout_duration = DATE_DIFF(l.checkout_date, l.returned_date, "days")
    LET book = (FOR bib IN BookInBranch
        FILTER bib.book == l.book
        RETURN bib)[0].book
    COLLECT isbn = book.isbn INTO books
    LET book_duration = SUM(books[*].checkout_duration)
    SORT book_duration DESC
    LIMIT 1
    RETURN {isbn: isbn, duration: book_duration}
