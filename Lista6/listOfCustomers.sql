FOR c IN Customer
    FOR bc IN BranchCustomer
        FILTER bc.branch == <branch_id>
        FILTER bc.customer == c._id
        COLLECT WITH COUNT INTO count
        RETURN count
