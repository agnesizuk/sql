FOR b IN Branch
    LET branch_borrowers = (
        FOR bl IN BorrowerLoan
            FILTER bl.branch == b._id
            RETURN bl.borrower
    )
    LET borrower_counts = (
        FOR borrower IN branch_borrowers
            COLLECT withCount = COUNT INTO borrowers
            SORT withCount DESC
            RETURN {borrower: borrower, count: withCount}
    )
    LET top_borrowers = (
        FOR i IN 0..2
            FILTER i < LENGTH(borrower_counts)
            RETURN borrower_counts[i]
    )
    RETURN {branch: b, top_borrowers: top_borrowers}
