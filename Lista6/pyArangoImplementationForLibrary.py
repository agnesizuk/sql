from pyArango.connection import *
from pyArango.graph import *

# Connect to the ArangoDB server
conn = Connection(username="root", password="password")


db = conn.createDatabase(name="library_network")


class Branches(Collection):
    _fields = {
        "name": Field(),
        "address": Field(),
        "phone": Field()
    }


class Books(Collection):
    _fields = {
        "title": Field(),
        "author": Field(),
        "isbn": Field(),
        "publisher": Field()
    }


class Borrowers(Collection):
    _fields = {
        "name": Field(),
        "email": Field(),
        "phone": Field()
    }


class Loans(Collection):
    _fields = {
        "book_id": Field(),
        "borrower_id": Field(),
        "branch_id": Field(),
        "due_date": Field(),
        "returned_date": Field()
    }


class Reservations(Collection):
    _fields = {
        "book_id": Field(),
        "borrower_id": Field(),
        "branch_id": Field(),
        "reservation_date": Field(),
        "pickup_date": Field(),
        "expiration_date": Field()
    }


db.createCollection("Branches")
db.createCollection("Books")
db.createCollection("Borrowers")
db.createCollection("Loans")
db.createCollection("Reservations")


branches_to_books = db.createGraph(name="BranchesToBooks")
borrowers_to_loans = db.createGraph(name="BorrowersToLoans")
books_to_reservations = db.createGraph(name="BooksToReservations")


# Add the collections to graphs - pro tip

branches_to_books.createVertexType("Branches")
branches_to_books.createVertexType("Books")

borrowers_to_loans.createVertexType("Borrowers")
borrowers_to_loans.createVertexType("Loans")

books_to_reservations.createVertexType("Books")
books_to_reservations.createVertexType("Borrowers")
books_to_reservations.createVertexType("Reservations")


class BranchesToBooksEdge(Edge):
    _fields = {
        "location": Field()
    }

class BorrowersToLoansEdge(Edge):
    pass

class BooksToReservationsEdge(Edge):
    pass

# graph

branches_to_books.createEdgeDefinition(
    edgeCollection="BranchesToBooksEdge",
    fromCollections=["Branches"],
    toCollections=["Books"]
)

borrowers_to_loans.createEdgeDefinition(
    edgeCollection="BorrowersToLoansEdge",
    fromCollections=["Borrowers"],
    toCollections=["Loans"]
)

books_to_reservations.createEdgeDefinition(
    edgeCollection="BooksToReservationsEdge",
    fromCollections=["Books"],
    toCollections=["Borrowers", "Reservations"]
)
